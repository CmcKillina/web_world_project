=============================================
WEB WORLD i.e. W(D1)

D1: [[1, 2, 1, 1], [2, 3, 2, 2], [3, 4, 1, 1], [4, 5, 2, 1]]
D2: [[1, 2, 1, 1], [2, 3, 2, 1], [3, 4, 2, 1], [4, 5, 2, 1]]
D3: [[1, 2, 1, 1], [2, 3, 2, 1], [3, 4, 2, 2], [4, 5, 1, 1]]
D4: [[1, 2, 1, 1], [2, 3, 2, 2], [3, 4, 1, 2], [4, 5, 1, 1]]
D5: [[1, 2, 1, 2], [2, 3, 1, 1], [3, 4, 2, 1], [4, 5, 2, 1]]
D6: [[1, 2, 1, 2], [2, 3, 1, 1], [3, 4, 2, 2], [4, 5, 1, 1]]
D7: [[1, 2, 1, 2], [2, 3, 1, 2], [3, 4, 1, 1], [4, 5, 2, 1]]
D8: [[1, 2, 1, 2], [2, 3, 1, 2], [3, 4, 1, 2], [4, 5, 1, 1]]

=============================================
WEB COLOURING AND MIXING MATRICES
Note: Position in Matrix: [Row,Column] 
      Web Colouring Matrix Entry: WC 
      Web Mixing Matrix Entry: WM

[D1, D1] - (WC = x + 6*x^2 + 10*x^3 + 5*x^4) (WM = 1/12)
[D1, D2] - (WC = x^2 + 2*x^3 + x^4) (WM = -1/12)
[D1, D3] - (WC = x^2 + 4*x^3 + 3*x^4) (WM = 1/12)
[D1, D4] - (WC = 2*x^2 + 5*x^3 + 3*x^4) (WM = -1/12)
[D1, D5] - (WC = x^2 + 4*x^3 + 3*x^4) (WM = 1/12)
[D1, D6] - (WC = x^2 + 5*x^3 + 5*x^4) (WM = -1/12)
[D1, D7] - (WC = 2*x^2 + 5*x^3 + 3*x^4) (WM = -1/12)
[D1, D8] - (WC = x^3 + x^4) (WM = 1/12)

[D2, D1] - (WC = 4*x^2 + 9*x^3 + 5*x^4) (WM = -1/4)
[D2, D2] - (WC = x + 3*x^2 + 3*x^3 + x^4) (WM = 1/4)
[D2, D3] - (WC = 3*x^2 + 6*x^3 + 3*x^4) (WM = -1/4)
[D2, D4] - (WC = 3*x^3 + 3*x^4) (WM = 1/4)
[D2, D5] - (WC = 3*x^2 + 6*x^3 + 3*x^4) (WM = -1/4)
[D2, D6] - (WC = x^2 + 6*x^3 + 5*x^4) (WM = 1/4)
[D2, D7] - (WC = 3*x^3 + 3*x^4) (WM = 1/4)
[D2, D8] - (WC = x^4) (WM = -1/4)

[D3, D1] - (WC = 2*x^2 + 7*x^3 + 5*x^4) (WM = 1/12)
[D3, D2] - (WC = x^2 + 2*x^3 + x^4) (WM = -1/12)
[D3, D3] - (WC = x + 5*x^2 + 7*x^3 + 3*x^4) (WM = 1/12)
[D3, D4] - (WC = 2*x^2 + 5*x^3 + 3*x^4) (WM = -1/12)
[D3, D5] - (WC = x^2 + 4*x^3 + 3*x^4) (WM = 1/12)
[D3, D6] - (WC = 3*x^2 + 8*x^3 + 5*x^4) (WM = -1/12)
[D3, D7] - (WC = 2*x^3 + 3*x^4) (WM = -1/12)
[D3, D8] - (WC = x^3 + x^4) (WM = 1/12)

[D4, D1] - (WC = 3*x^2 + 8*x^3 + 5*x^4) (WM = -1/12)
[D4, D2] - (WC = x^3 + x^4) (WM = 1/12)
[D4, D3] - (WC = 2*x^2 + 5*x^3 + 3*x^4) (WM = -1/12)
[D4, D4] - (WC = x + 5*x^2 + 7*x^3 + 3*x^4) (WM = 1/12)
[D4, D5] - (WC = 2*x^3 + 3*x^4) (WM = -1/12)
[D4, D6] - (WC = 2*x^2 + 7*x^3 + 5*x^4) (WM = 1/12)
[D4, D7] - (WC = x^2 + 4*x^3 + 3*x^4) (WM = 1/12)
[D4, D8] - (WC = x^2 + 2*x^3 + x^4) (WM = -1/12)

[D5, D1] - (WC = 2*x^2 + 7*x^3 + 5*x^4) (WM = 1/12)
[D5, D2] - (WC = x^2 + 2*x^3 + x^4) (WM = -1/12)
[D5, D3] - (WC = x^2 + 4*x^3 + 3*x^4) (WM = 1/12)
[D5, D4] - (WC = 2*x^3 + 3*x^4) (WM = -1/12)
[D5, D5] - (WC = x + 5*x^2 + 7*x^3 + 3*x^4) (WM = 1/12)
[D5, D6] - (WC = 3*x^2 + 8*x^3 + 5*x^4) (WM = -1/12)
[D5, D7] - (WC = 2*x^2 + 5*x^3 + 3*x^4) (WM = -1/12)
[D5, D8] - (WC = x^3 + x^4) (WM = 1/12)

[D6, D1] - (WC = x^2 + 5*x^3 + 5*x^4) (WM = -1/12)
[D6, D2] - (WC = x^3 + x^4) (WM = 1/12)
[D6, D3] - (WC = 2*x^2 + 5*x^3 + 3*x^4) (WM = -1/12)
[D6, D4] - (WC = x^2 + 4*x^3 + 3*x^4) (WM = 1/12)
[D6, D5] - (WC = 2*x^2 + 5*x^3 + 3*x^4) (WM = -1/12)
[D6, D6] - (WC = x + 6*x^2 + 10*x^3 + 5*x^4) (WM = 1/12)
[D6, D7] - (WC = x^2 + 4*x^3 + 3*x^4) (WM = 1/12)
[D6, D8] - (WC = x^2 + 2*x^3 + x^4) (WM = -1/12)

[D7, D1] - (WC = 3*x^2 + 8*x^3 + 5*x^4) (WM = -1/12)
[D7, D2] - (WC = x^3 + x^4) (WM = 1/12)
[D7, D3] - (WC = 2*x^3 + 3*x^4) (WM = -1/12)
[D7, D4] - (WC = x^2 + 4*x^3 + 3*x^4) (WM = 1/12)
[D7, D5] - (WC = 2*x^2 + 5*x^3 + 3*x^4) (WM = -1/12)
[D7, D6] - (WC = 2*x^2 + 7*x^3 + 5*x^4) (WM = 1/12)
[D7, D7] - (WC = x + 5*x^2 + 7*x^3 + 3*x^4) (WM = 1/12)
[D7, D8] - (WC = x^2 + 2*x^3 + x^4) (WM = -1/12)

[D8, D1] - (WC = x^2 + 6*x^3 + 5*x^4) (WM = 1/4)
[D8, D2] - (WC = x^4) (WM = -1/4)
[D8, D3] - (WC = 3*x^3 + 3*x^4) (WM = 1/4)
[D8, D4] - (WC = 3*x^2 + 6*x^3 + 3*x^4) (WM = -1/4)
[D8, D5] - (WC = 3*x^3 + 3*x^4) (WM = 1/4)
[D8, D6] - (WC = 4*x^2 + 9*x^3 + 5*x^4) (WM = -1/4)
[D8, D7] - (WC = 3*x^2 + 6*x^3 + 3*x^4) (WM = -1/4)
[D8, D8] - (WC = x + 3*x^2 + 3*x^3 + x^4) (WM = 1/4)

=============================================
WEB COLOURING MATRIX TRACE: 8*x + 38*x^2 + 54*x^3 + 24*x^4

ALL WEB COLOURING ROW SUMS EQUAL x + 14*x^2 + 36*x^3 + 24*x^4: True

=============================================
WEB MIXING MATRIX TRACE: 1

ALL WEB MIXING ROW SUMS EQUAL 0: True

=============================================
Program Running Time (Hour:Minutes:Seconds:Milliseconds)
0:00:00.186477
=============================================
