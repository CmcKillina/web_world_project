# README #

******************
Setup Instructions

A Linux Operating System (Not available on Windows or Mac OS)
System Requirements
SageMath Package and Python 2.7

1) Pull this repository to a local directory
2) Navigate to this directory and open up a Sage sub-shell by entering the command:

sage -sh

3)Now initialize the "run" directory by entering the command

python run

4)You should be presented with the main menu for the software.

*********************
PyPi Package

The software is also available on PyPi.
It can be installed into your Python package using the command

pip install WebWorlds

*********************
