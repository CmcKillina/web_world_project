From the Main Menu of the program you have some options 

You can enter a Web Diagram in the standard format for Web Diagrams [[a,b,c,d].....] like so and 
press "Enter" to begin calculations.

OR

You can enter either "1", "2", "3", "4". These are pre-made diagrams shown in the main menu as example inputs. 

OR 

You can enter "5" which will bring up a list of all the previously saved .txt file results saved in previous calculations

If you opt to save results to file, the results (.png and .txt) are saved in the directory where you have placed the "Run" directory. 




